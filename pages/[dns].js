import { cvToHex, cvToValue, parseReadOnlyResponse, stringAsciiCV } from "@stacks/transactions";
import Head from "next/head";
import ProfileNotFound from "../components/public/profileNotFound";
import PublicProfile from "../components/public/publicProfile";
import { getContractName, getContractOwner, getStacksAPIPrefix } from "../services/auth";


export default function OwlLink(data) {
    // Get profile data
    let owlLinkProfile = data["data"];
    let dns = data["dns"];
    let profileFound = data["profileFound"];

    // Show based on profile availability
    return (
        <>
            {/* Header */}
            {
                profileFound ?
                    // If profile is found
                    <Head>
                        <title>{(owlLinkProfile["name"] ? owlLinkProfile["name"] : ("@" + dns)) + " | Owl Link"}</title>
                        <meta name="description" content={owlLinkProfile["bio"] ? owlLinkProfile["bio"] : ""} />

                        <meta name="robots" content="index,follow" />

                        {/* Fav icon */}
                        <link rel="icon" href={owlLinkProfile["img"] ? owlLinkProfile["img"] : "/favicon.ico"} />

                        {/* Facebook Meta Tags */}
                        <meta property="og:type" content="website" />
                        <meta property="og:title" content={owlLinkProfile["name"] ? owlLinkProfile["name"] : ("@" + dns)} />
                        <meta property="og:description"
                            content={owlLinkProfile["bio"] ? owlLinkProfile["bio"] : ""} />
                        <meta property="og:url" content="https://owl.link/" />
                        <meta property="og:image" content={owlLinkProfile["img"] ? owlLinkProfile["img"] : "https://owl.link/images/logo/owllink.png"} />
                        <meta property="og:site_name" content="owllink" />

                        {/* Twitter Meta Tags */}
                        <meta name="twitter:card" content="summary" />
                        <meta name="twitter:title" content={owlLinkProfile["name"] ? owlLinkProfile["name"] : ("@" + dns)} />
                        <meta name="twitter:description"
                            content={owlLinkProfile["bio"] ? owlLinkProfile["bio"] : ""} />
                        <meta name="twitter:url" content="https://owl.link/" />
                        <meta name="twitter:image" content={owlLinkProfile["img"] ? owlLinkProfile["img"] : "https://owl.link/images/logo/owllink.png"} />
                        <meta name="twitter:site" content="@owllink_btc" />
                    </Head>
                    :
                    // If profile is not found
                    <Head>
                        <title>Page not found | OwlLink</title>
                        <meta name="description" content="Showcase your collectibles, NFTs, and social profiles. Everything is in one place." />
                    </Head>
            }

            {/* Body */}
            {
                profileFound ?
                    // If profile is found
                    <PublicProfile owlLinkProfile={owlLinkProfile} dns={dns}></PublicProfile>
                    :
                    // If profile is not found
                    <ProfileNotFound dns={dns}></ProfileNotFound>
            }
        </>
    );
}

// This gets called on every request
export async function getServerSideProps(context) {
    // Get path param
    const { params } = context;
    const { dns } = params;
    let profile = {};
    let profileFound = false;

    try {
        // If dns is available, then proceed
        if (dns) {
            // Fetch gaia URL from stacks blockchain
            const rawResponse = await fetch(getStacksAPIPrefix() + '/v2/contracts/call-read/' + getContractOwner() +
                '/' + getContractName() + '/get-token-uri', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "sender": getContractOwner(),
                    "arguments": [
                        cvToHex(stringAsciiCV(dns))
                    ]
                })
            });
            const content = await rawResponse.json();

            // If data found on stacks blockchain
            if (content && content.okay) {
                // Get gaia address
                const gaiaProfileUrl = cvToValue(parseReadOnlyResponse(content)).value.value;

                try {
                    // Fetch data from external API
                    const res = await fetch(gaiaProfileUrl);
                    profile = await res.json();

                    // Update default NFT images

                    // Turn the flag on
                    profileFound = true;
                } catch (error) {
                    // Error reading file from gaia, but btc domain already minted.

                    // Turn the flag on
                    profileFound = true;
                }
            }
        }
    } catch (error) {
        // Turn the flag off
        profileFound = false;
    }

    // Pass data to the page via props
    return {
        props: {
            "data": profile,
            "dns": dns,
            "profileFound": profileFound
        }
    };
}