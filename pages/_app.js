import '../styles/globals.css';
import '../styles/custom.css';

import SSRProvider from 'react-bootstrap/SSRProvider';
import Head from 'next/head';

function MyApp({ Component, pageProps }) {
  return (
    <>
      <SSRProvider>
        {/* Header */}
        <Head>
          {/* Fathom Analytics */}
          <script src="https://honorable-animal.owl.link/script.js" data-site="DTVEMVZJ" defer data-included-domains="owl.link"></script>
        </Head>

        {/* Body */}
        <Component {...pageProps} />
      </SSRProvider>
    </>
  )
}

export default MyApp
