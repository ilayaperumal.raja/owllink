import { Pagination } from "react-bootstrap";

export default function NftPopupPagination(props) {
    const pages = props["pages"];
    const getNftsImageObjFromGaia = props["getNftsImageObjFromGaia"];
    const activePage = props["nftsPaginationActivePage"];
    const setActivePage = props["setNftsPaginationActivePage"];

    // Variables
    let items = [];

    /**
     * Pagination items
     * 
     * @returns 
     */
    function getPaginationItem() {
        if (pages && pages > 0) {
            for (let number = 1; number <= pages; number++) {
                items.push(
                    <Pagination.Item key={number} active={number === activePage} onClick={() => { getNftsImageObjFromGaia(number) }}>
                        {number}
                    </Pagination.Item>
                );
            }
        }

        return items;
    }

    return (
        <>
            <Pagination>{getPaginationItem()}</Pagination>
        </>
    )
}